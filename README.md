## biblatex-scinet
- - -

### Bibliography style for scientific.net

The `scinet` BibLaTeX style implements the citation style of scientfic.net, as to be seen in the example document provided at https://www.scientific.net/Downloads . It is based on ´´.

Requirements are:

1. ...

- - -

### Installation in TeX Live

Copy the files <tt>scinet.cbx</tt>, <tt>scinet.bbx</tt>, <tt>scinet-draft.cbx</tt> and <tt>scinet-draft.bbx</tt> to <tt>~/texmf/tex/latex/biblatex-scinet/</tt>.

If not yet existing, the directory tree has to be created and made user readable.

### Installation in MacTeX

Copy the files <tt>scinet.cbx</tt>, <tt>scinet.bbx</tt>, <tt>scinet-draft.cbx</tt> and <tt>scinet-draft.bbx</tt> to <tt>~/Library/texmf/tex/latex/biblatex-scinet/</tt>.

If not yet existing, the directory tree has to be created and made user readable.

### Installation in MikTeX

1. Create a private tree, e.g. under <tt>~/texmf</tt> (has to be outside of the MikTeX install path)

2. Copy the files <tt>scinet.cbx</tt>, <tt>scinet.bbx</tt>, <tt>scinet-draft.cbx</tt> and <tt>scinet-draft.bbx</tt> to <tt>~/texmf/tex/latex/biblatex-scinet/</tt>.

3. Register the private tree as 'Root':
	1. MikTeX Settings
	2. Category 'Roots'
	3. Click 'Add..'
	4. Select directory named like given above
	5. Change search order, if necessary
	6. Click 'Apply'

- - -

### Platforms

Independent

Tested with TeX Live 2017 in Ubuntu 16.04 LTS along with the LaTeX template provided by scientific.net at https://www.scientific.net/Downloads.

### License

The `scinet` style is licensed under the terms of **LPPL 1.3**.

- - -

E. Frank Sandig
2018-03-12
