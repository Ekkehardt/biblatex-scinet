## biblatex-scinet: Changelog
###### E. Frank Sandig -- 2018-03-12
- - -
### 0.10 - 2018-03-12
 - some functionality

####### TODO
 - numeric citation without abbreviation of numbers
 - ragged blocks
 - article: V1.V2. Name, Title, Journ. Vol (year) pages.
 - book:    V1.V2. Name, Title, Edition, Publisher, Place, year
 - chapter: V1.V2. Name, Title, in: Editors (Eds), booktiotle, publisher, place, year, Pages
 - NO markup in citations!
